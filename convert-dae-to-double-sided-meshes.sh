#!/bin/bash -e
# ------------------------------------------------------------------
INFO_TEXT="Convert all COLLADA (*.dae) files in a given folder so that they have \n" 
INFO_TEXT+="double sided meshes.\n"
INFO_TEXT+="The trick is to add a special <extra> node to every material. This node.\n"
INFO_TEXT+="is understood by default by threejs.\n"

display_usage() {
  echo -e "\n$INFO_TEXT"
  echo -e "\nUsage:\n$0 model_to_convert_folder\n"
}

if [[ $# == 0 ]]; then
  display_usage
  exit 1
fi

find $1 -name '*.dae' -print0 | xargs -I {} -0 xsltproc -o {} convert-dae-to-double-sided-meshes.xslt {}