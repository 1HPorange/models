<?xml version="1.0"?>
<model xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.humanbrainproject.eu/SP10/2017/robot_model_config" xsi:schemaLocation="http://schemas.humanbrainproject.eu/SP10/2017/robot_model_config ../robot_model_configuration.xsd">
  <name>HBP Mouse v2</name>
  <version>2.0</version>
  <maturity>production</maturity>
  <thumbnail>thumbnail.png</thumbnail>
  <sdf version="1.6">model.sdf</sdf>

  <author>
   <name>Fabian Aichele</name>
   <email>aichele@fzi.de</email>
  </author>

   <description>
    Simplified model of a real mouse. Each of the four limbs of the mouse model can be actuated individually using PID controller instances for foot, elbow/knee and shoulder/hip joints. The model is intended for usage with a CPG controller.
  </description>

  <documentation>
      <sensors>
        <sensor name="cam_left_eye" type="camera" />
        <sensor name="cam_right_eye" type="camera" />
      </sensors>
      <actuators>
        <actuator name="shoulder_L_joint" type="motor" />
        <actuator name="upper_arm_L_joint" type="motor" />
        <actuator name="forearm_L_joint" type="motor" />
        <actuator name="wrist_L_joint" type="motor" />
        <actuator name="shoulder_R_joint" type="motor" />
        <actuator name="upper_arm_R_joint" type="motor" />
        <actuator name="forearm_R_joint" type="motor" />
        <actuator name="wrist_R_joint" type="motor" />
        <actuator name="thigh_L_joint" type="motor" />
        <actuator name="shin_L_joint" type="motor" />
        <actuator name="shin_lower_L_joint" type="motor" />
        <actuator name="thigh_R_joint" type="motor" />
        <actuator name="shin_R_joint" type="motor" />
        <actuator name="shin_lower_R_joint" type="motor" />
      </actuators>
  </documentation>
</model>
