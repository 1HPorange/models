This package contains robot models for ROS and the GAZEBO simulator. Futhermore
a build script used by CI (Jenkins) to produce two RPM packages, namely one for
the models (hbp-gazebo-models-<version>.noarch.rpm) and one for configuration
files (hbp-configs-<version>.norach.rpm).

Authors: Stefan Deser, Bernd Eckstein

Make sure your model are double sided
=====================================

Collada specification does not explicitly define if meshes are double-sided.
When exporting Collada from Blender, you may end up with some transparent faces in
ThreeJS.

The script convert-dae-to-double-sided-meshes.sh could be use to avoid this. This
script will make sure that an <extra> tag is added to every materials so that threeJS
can render the two sides of every meshes.

It is largely inspired from this post:
http://stackoverflow.com/questions/11770052/collada-model-faces-not-displaying-correctly-in-three-js


Install the models
==================


1. For developers on Ubuntu
----------------------------
This script creates symlinks in ~/.gazebo/models
> ./create-symlinks.sh

When creating a new robot, please verify the validity of the robot configuarion by running
> xmllint -schema $HBP/Models/robot_model_configuration.xsd myrobot.config} --noout

2. For deployment on Redhat Linux
----------------------------------
The models and configuration packages are built by the script:

    _rpmbuild/build-rpm-package.sh

It is used by Jenkins CI to automatically build the packages and also to upload
them to a RPM repository where they can later be installed (e.g. via puppet).

This script takes all models defined in "_rpmbuild/models.txt" and includes them
in the models RPM package, which will be named "hbp-gazebo-models-<version>.noarch.rpm".
So if you want your model to be included make sure it is contained in "models.txt"!
(Note that the webification of your model is done automatically.)

If you make a change (or include, exclude a model) please make sure to also adjust
the version number in the particular .spec-file:

    _rpmbuild/hbp-configs.spec
    _rpmbuild/hbp-gazebo-models.spec

It must always be ensured that each version of the package maps to a particular
set of contents. It must not be the case that there is a package version used for
two different content sets! Also it must not be that there are two version numbers
refering to the exact same content set!

If this principle (unambiguous mapping of version number to content set and vice
versa) is violated configuration management and traceability in the case of bugs
is made much more difficult or even impossible.